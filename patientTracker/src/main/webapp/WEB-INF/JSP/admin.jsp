<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin - Dashboard</title>
    <link rel="stylesheet" href="/CSS/admin.css">
</head>
<body>
    <div class = "container">
        <div class ="navigation">
        <ul>
            <li>
                <a href="#">
                    <span class ="icon"><ion-icon name="medkit-outline"></ion-icon></span>
                    <span class ="title">Hospital</span>
                </a>
            </li>
            <li>
                <a href="/admin">
                    <span class ="icon"><ion-icon name="home-outline"></ion-icon></span>
                    <span class ="title">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="/admin/addDoctor">
                    <span class ="icon"><ion-icon name="person-add-outline"></ion-icon></span>
                    <span class ="title">Add Doctor</span>
                </a>
            </li>
            <li>
                <a href="/admin/getAdmin">
                    <span class ="icon"><ion-icon name="person-add-outline"></ion-icon></span>
                    <span class ="title">All Admins</span>
                </a>
            </li>            
                        
            <li>
                <a href="/admin/section">
                    <span class ="icon"><ion-icon name="add-circle-outline"></ion-icon></span>
                    <span class ="title">Sections</span>
                </a>
            </li>
            <li>
                <a href="/admin/contactinfo">
                    <span class ="icon"><ion-icon name="call-outline"></ion-icon></span>
                    <span class ="title">Contact Us Info</span>
                </a>
            </li>
            <li>
                <a href="/signout">
                    <span class ="icon"><ion-icon name="log-out-outline"></ion-icon></span>
                    <span class ="title">Sign Out</span>
                </a>
            </li>
        </ul>
       </div>
       <!-- main -->
       <div class = "main">
           <div class="topbar">
               <div class="toggle">
                <ion-icon name="menu-outline"></ion-icon>
               </div>
               <!-- search -->
                <div class="search">
                <label>
                    <input type ="text" placeholder="Search here">
                    <ion-icon name="search-outline"></ion-icon>
                </label>
               </div>
                <!-- userImg -->

                <div class="topnav-right">
                    <div class="dropdown">
                        <button class="dropbtn">
                            <img src="./Images/user.png">
                        </button>
                        <div class="dropdown-content">
                            <a href="#"><ion-icon name="person-circle-outline"></ion-icon><span>My Profile</span></a>
                            <a href="#"><ion-icon name="create-outline"></ion-icon><span>Edit Account</span></a>
                            <a href="#"><ion-icon name="help-circle-outline"></ion-icon> <span>Help</span></a>
                        </div>
                    </div>
                    <div class="drName" value ="Admin">
                       <!-- Dr. Chandan Kumar-->
                        ${Admin.username} 
                    </div>     
    
                </div>
                        
           </div>
           <!-- cards -->
           <div class="cardBox">
               <div class="card">
                   <div>
                       <div class="numbers">${ListDoctor.size()}</div>
                       <div class="cardName">Doctors</div>
                   </div>
                   <div class ="iconBx">
                       <ion-icon name="person-add-outline"></ion-icon>
                   </div>
               </div>
               <div class="card">
                <div>
                    <div class="numbers">1</div>
                    <div class="cardName">Comments</div>
                </div>
                <div class ="iconBx">
                    <ion-icon name="chatbubbles-outline"></ion-icon>
                </div>
            </div>
           </div>
           <!-- All doctors list -->
           <div class="details">
               <table>
                   <thead>
                        <tr id="header">
                            <th>Id</th>
                            <th>Doctor Name</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Phone</th>
                            <th>Gender</th>
                            <th>Section</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                         <!--<tr each="doctor : ${ListDoctor}">
                          <td value =doctor.getId()></td> 
                          <td value=doctor.getName()></td> 
                          <td value=doctor.getEmail()></td> 
                          <td value=doctor.getUsername()></td> 
                          <td value=doctor.getPassword()></td> 
                          <td value=doctor.getPhone()></td>    
                          <td value=doctor.getGender()></td>    
                          <td value=doctor.getSection()></td>        
                          <td> 
                             <a href="@{'/edit/'+${doctor.id}}">Edit</a>
                          </td>
                          <td>
                             <a href="@{'/delete/'+${doctor.id}}">Delete</a>
                          </td>
                         </tr>-->
                      <c:forEach var="doct" items="${ListDoctor}">
                       <tr>
                       <td>${doct.id}</td>
                       <td>${doct.name}</td>                       
                       <td>${doct.email}</td>                       
                       <td>${doct.username}</td>                       
                       <td>${doct.password}</td>                       
                       <td>${doct.phone}</td>                       
                       <td>${doct.gender}</td>                       
                       <td>${doct.section}</td>
                       <td>
                       <a href="/edit/${doct.id}">Edit</a> 
                       <a href="/delete/${doct.id}">Delete</a>                         
                       </td>                     
                       </tr>
                       </c:forEach>
                      </tbody>
                </table>
           </div>
       </div>

    </div>

    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    
    <script>
        // MenuToggle
        let toggle = document.querySelector('.toggle');
        let navigation = document.querySelector('.navigation');
        let main = document.querySelector('.main');

        toggle.onclick = function()
        {
            navigation.classList.toggle('active');
            main.classList.toggle('active');
        }

        // add hovered class in selected list item
        let list = document.querySelectorAll('.navagation li');
        function activelink(){
            list.forEach((item) =>
            item.classlist.remove('hovered'));
            this.classlist.add('hovered')
        }
        list.forEach((item)=>
        item.addEventListener('mouseover',activelink));
    </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/CSS/login.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
  </head>
  <body>
    <div class="container">
      <div class="wrapper">
        <div class="title"><span>${Title}</span></div>
        <form action="/sucessfully-login" method="POST">
          <div class="row">
            <i class="fas fa-at"></i>
            <input type="text" name="email" id="email" placeholder="Email Id" required>
          </div>
          <div class="row">
            <i class="fas fa-user"></i>
            <input type="text" name="username" id="username" placeholder="Username" required>
          </div>
          <div class="row">
            <i class="fas fa-lock"></i>
            <input type="password" name="password" id="password" placeholder="Password" required>
          </div>
          <div class="row button">
            <input type="submit" value="Login">
          </div>
          <div class="signup-link">Not a member? <a href="/admin/addAdmin">Signup now</a></div>
        </form>
      </div>
    </div>

  </body>
</html>
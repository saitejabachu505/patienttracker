<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Us</title>
    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap"
    rel="stylesheet">
    <link rel="stylesheet" href="/CSS/contactus.css"><link>

</head>
<body>
    <div class = "container">
        <div class="contact-box"> 
            <div class="left">
                <h2 id="reach">Reach Us</h2>
                <table>
                    <tr>
                        <td>Email</td>
                        <td>contactus@gmail.com</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>+1 0123456789</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>#212, Gronf floor,7th cross<br>
                        some layout, some road,<br>
                        Talegaon , Pune 410507
                    </td>
                    </tr>
                    <tr>
                        <td>Work hours</td>
                        <td>Mon-Fri 8:00 AM to 6:00 PM</td>
                    </tr>
                </table>
            </div>
    
            <div class="right">
                 <h2 id="contact">${Contact}</h2>
                 <input type="text" class="field" placeholder="Your Name">
                 <input type="text" class="field" placeholder="Your Email">
                 <input type="text" class="field" placeholder="Phone">
                 <textarea placeholder="Message" class="field"></textarea>
                 <button class="btn">Send</button>
            </div>
        </div>
    </div>    
    
</body>
</html>
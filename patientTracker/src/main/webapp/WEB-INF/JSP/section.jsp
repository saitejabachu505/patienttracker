<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/CSS/section.css">
    <title>Sections</title>
</head>
<body>
        <!--  sectionswise doctors list  -->
        <div id="sections">
            <div class="section">   
               <div class="tables">
                <table>
                    <thead>
                         <tr id="ttitle">
                             <th>${D}</th>
                             <th></th>
                             <th></th>
                             <th></th>
                        </tr>
                         <tr id="header">
                             <th>Name</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                         </tr>
                     </tbody>
                 </table>
            </div>

             <div class="tables">
                <table>
                    <thead>
                         <tr id="ttitle1">
                             <th>Fecalysis</th>
                             <th></th>
                             <th></th>
                             <th></th>
                        </tr>
                         <tr id="header1">
                             <th>Name</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                         </tr>
                     </tbody>
                 </table>
            </div>
        </div>
        
        <div class="section">
              <div class="tables">
                <table>
                    <thead>
                         <tr id="ttitle2">
                             <th>Hemathology</th>
                             <th></th>
                             <th></th>
                             <th></th>
                        </tr>
                         <tr id="header2">
                             <th>Name</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                         </tr>
                     </tbody>
                 </table>
            </div>
            <div class="tables">
                <table>
                    <thead>
                         <tr id="ttitle3">
                             <th>Xray</th>
                             <th></th>
                             <th></th>
                             <th></th>
                        </tr>
                         <tr id="header3">
                             <th>Name</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                         </tr>
                     </tbody>
                 </table>
            </div>
        </div>

        <div class="section"> 
            <div class="tables">
                <table>
                    <thead>
                         <tr id="ttitle4">
                             <th>Reahabilitation</th>
                             <th></th>
                             <th></th>
                             <th></th>
                        </tr>
                         <tr id="header4">
                             <th>Name</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                         </tr>
                     </tbody>
                 </table>
            </div>
            <div class="tables">
                <table>
                    <thead>
                         <tr id="ttitle5">
                             <th>Sputum</th>
                             <th></th>
                             <th></th>
                             <th></th>
                        </tr>
                         <tr id="header5">
                             <th>Name</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                         </tr>
                     </tbody>
                 </table>
            </div>
        </div>

        <div class="section">
            <div class="tables">
                <table>
                    <thead>
                         <tr id="ttitle6">
                             <th>Urinalysis</th>
                             <th></th>
                             <th></th>
                             <th></th>
                        </tr>
                         <tr id="header6">
                             <th>Name</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                         </tr>
                     </tbody>
                 </table>
            </div>
            <div class="tables">
                <table>
                    <thead>
                         <tr id="ttitle7">
                             <th>Other</th>
                             <th></th>
                             <th></th>
                             <th></th>
                        </tr>
                         <tr id="header7">
                             <th>Name</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                         </tr>
                     </tbody>
                 </table>
            </div>
        </div>
    </div>
</body>
</html>
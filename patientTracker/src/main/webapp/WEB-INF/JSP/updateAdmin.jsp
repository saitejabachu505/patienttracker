<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Registeration </title>
    <link rel="stylesheet" href="/CSS/newadmin.css" type= "text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>

</head>
<body>
    <div class="container">
        <div class="regform">
            <div class="title"><span>${Title}</span></div>
                  <form action="/sucessfully-updatea" method="POST">
                    <div class="row">
                        <i class="fas fa-user"></i>
                        <input type="number" value="${Admin.id}" name="id" id="id" placeholder="Id" required>
                    </div>

                    <div class="row">
                        <i class="fas fa-user"></i>
                        <input type="text" value="${Admin.name}" name="name" id="name" placeholder="Name" required>
                    </div>
                    <div class="row">
                        <i class="fas fa-at"></i>
                        <input type="text"  value="${Admin.email}" name="email" id="email" placeholder="Email" required>
                    </div>
                    <div class="row">
                        <i class="fas fa-user"></i>
                        <input type="text" value="${Admin.username}" name="username" id="username" placeholder="Username" required>
                    </div>
                    <div class="row">
                        <i class="fas fa-lock"></i>
                        <input type="text"  value="${Admin.password}" name="password" id="password" placeholder="Password" required>
                    </div>
                    <div class="row">
                        <i class="fas fa-lock"></i>
                        <input type="text" value="${Admin.confirm_password}" name="confirm_password" id="confirm_password" placeholder="Confirm-password" required>
                    </div>
                    <div class="row">
                        <i class="fas fa-phone"></i>
                        <input type="text" value="${Admin.phone}" name="phone" id="phone" placeholder="Phone" required>
                    </div>   
                    <div class="row">
                        <i class="fas fa-angle-right"></i>
                        <select class="option" id="gender" name="gender">
                            <option disabled="disabled" selected="selected">${Admin.gender}</option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>   
                            
                    <div class="row button">
                        <input type="submit" value="Update">
                      </div>
                    
    
            </form>
        </div>
    
    </div>
</div>
</body>
</html>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/CSS/getAdmins.css">
    <title>Admins</title>
</head>
<body>
           <!-- All Admins list -->
               <div class="details">
                <table>
                    <thead>
                         <tr id="header">
                             <th>Id</th>
                             <th>Doctor Name</th>
                             <th>Email</th>
                             <th>Username</th>
                             <th>Password</th>
                             <th>Phone</th>
                             <th>Gender</th>
                             <th>Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                       <c:forEach var="admin" items="${ListAdmin}">
                        <tr>
                        <td>${admin.id}</td>
                        <td>${admin.name}</td>                       
                        <td>${admin.email}</td>                       
                        <td>${admin.username}</td>                       
                        <td>${admin.password}</td>                       
                        <td>${admin.phone}</td>                       
                        <td>${admin.gender}</td>                       
                        <td>
                        <a href="/edita/${admin.id}">Edit</a> 
                        <a href="/deletea/${admin.id}">Delete</a>                         
                        </td>                     
                        </tr>
                        </c:forEach>
                       </tbody>
                 </table>
            </div>
 
</body>
</html>
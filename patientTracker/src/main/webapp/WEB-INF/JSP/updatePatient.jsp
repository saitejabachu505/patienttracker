<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Patient Registeration Page</title>
    <link rel="stylesheet" href="/CSS/newpatient.css" type= "text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>

</head>
<body>
    <div class="container">
        <div class="regform">
            <div class="title"><span>${Title}</span></div>
                  <form action="/sucessfully-savep"  method="POST">
                    <div class="row">
                        <input type="number" value="${patient.itrno}" id="itrno" name="itrno" placeholder="ITR NO" required>
                    </div>
                    <div class="row">
                        <input type="text" value="${patient.name}" id="name" name="name" placeholder="Name" required>
                    </div>
                    <div class="row">
                        <input type="text" value="${patient.phone}" name="phone" id="phone" placeholder="Phone" required>
                    </div>
                    <div class="row">
                        <input type="text" value="${patient.joindate}" id="joindate" placeholder="Date " name="joindate" required>
                    </div>
                    <div class="row">
                        <input type="text" value="${patient.address}" id="address" placeholder="Address" name="address" required>    
                    </div>
                    <div class="row">
                        <input type="text" value="${patient.age}" id="age" name ="age" placeholder="Age" required>
                    </div>
                    <div class="row">
                        <select class="option" id="gender" name="gender">
                            <option disabled="disabled" selected="selected">${patient.gender}</option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>   
                    <div class="row">
                        <select class="option" id="status" name="status">
                            <option disabled="disabled" selected="selected">${patient.status}</option>
                            <option> Married</option>
                            <option> Un Married </option> 
                           </select>
                   </div>
                    <div class="row">
                        <input type="text" value="${patient.allergy}" id="allergy" name ="allergy" placeholder="Allergy" required>
                    </div>   

                    <div class="row">
                        <input type="text" value="${patient.disease}" id="disease" name ="disease" placeholder="Disease" required>
                    </div>   
                    <div class="row">
                        <input type="text" value="${patient.doctorname}" id ="doctorname" name="doctorname" placeholder="Doctor Name" required>
                    </div>   
                    <div class="row">
                        <input type="text" value="${patient.weight}" id="weight" name ="weight" placeholder="Weight" required>
                    </div>   
                    <div class="row">
                        <input type="text" value="${patient.height}" id="height" name="height" placeholder="Height" required>
                    </div>   
                    <div class="row">
                        <input type="text" value="${patient.bp}" id="bp" name="bp" placeholder="BP" required>
                    </div>
                    <div class="row button">
                        <input type="submit" value="Update">
                      </div>
                    
    
            </form>
        </div>
    
    </div>
</div>
  
</body>
</html>
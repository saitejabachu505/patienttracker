<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Doctor - Dashboard</title>
    <link rel="stylesheet" href="/CSS/doctorhome.css">
</head>
<body>
    <div class = "container">
        <div class ="navigation">
        <ul>
            <li>
                <a href="#">
                    <span class ="icon"><ion-icon name="medkit-outline"></ion-icon></span>
                    <span class ="title">Hospital</span>
                </a>
            </li>
            <li>
                <a href="/doctor">
                    <span class ="icon"><ion-icon name="home-outline"></ion-icon></span>
                    <span class ="title">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="/doctor/addpatient">
                    <span class ="icon"><ion-icon name="person-add-outline"></ion-icon></span>
                    <span class ="title">Add Paitent</span>
                </a>
            </li>
            <li>
                <a href="/doctor/contact">
                    <span class ="icon"><ion-icon name="call-outline"></ion-icon></span>
                    <span class ="title">Contact Us </span>
                </a>
            </li>
            <li>
                <a href="/signout">
                    <span class ="icon"><ion-icon name="log-out-outline"></ion-icon></span>
                    <span class ="title">Sign Out</span>
                </a>
            </li>
        </ul>
       </div>
       <!-- main -->
       <div class = "main">
           <div class="topbar">
               <div class="toggle">
                <ion-icon name="menu-outline"></ion-icon>
               </div>
               <!-- search -->
                <div class="search">
                <label>
                    <input type ="text" placeholder="Search here">
                    <ion-icon name="search-outline"></ion-icon>
                </label>
               </div>
                <!-- userImg -->

                <div class="topnav-right">
                    <div class="dropdown">
                        <button class="dropbtn">
                            <img src="./Images/user.png">
                        </button>
                        <div class="dropdown-content">
                            <a href="#"><ion-icon name="person-circle-outline"></ion-icon><span>My Profile</span></a>
                            <a href="#"><ion-icon name="create-outline"></ion-icon><span>Edit Account</span></a>
                            <a href="#"><ion-icon name="help-circle-outline"></ion-icon> <span>Help</span></a>
                        </div>
                    </div>
                    <div class="drName" Value="Doctor">
                       ${Doctor.username}
                    </div>     
    
                </div>
                        
           </div>
           <!-- cards -->
           <div class="cardBox">
               <div class="card">
                   <div>
                       <div class="numbers">${ListPatient.size()}</div>
                       <div class="cardName">Patients</div>
                   </div>
                   <div class ="iconBx">
                       <ion-icon name="person-add-outline"></ion-icon>
                   </div>
               </div>
               <div class="card">
                <div>
                    <div class="numbers">1</div>
                    <div class="cardName">Feedback</div>
                </div>
                <div class ="iconBx">
                    <ion-icon name="chatbubbles-outline"></ion-icon>
                </div>
            </div>
           </div>
            <!-- All doctors list -->
           <div class="details">
               <table>
                   <thead>
                        <tr id="header">
                            <th>ITR No</th>
                            <th>Name</th>
                            <th>Disease</th>
                            <th>Allergy</th>
                            <th>Gender</th>
                            <th>Phone</th>
                            <th>Gender</th>
                            <th>Doctor name</th>
                            <th>Edit</th>
                            <th>Delete</th>  
                        </tr>
                    </thead>
                    <tbody>
                       <c:forEach var="pat" items="${ListPatient}">
                       <tr>
                       <td>${pat.itrno}</td>
                       <td>${pat.name}</td>                       
                       <td>${pat.disease}</td>                       
                       <td>${pat.allergy}</td>                       
                       <td>${pat.gender}</td>                       
                       <td>${pat.phone}</td>                       
                       <td>${pat.gender}</td>                       
                       <td>${pat.doctorname}</td>
                       <td>
                       <a href="/editp/${pat.itrno}">Edit</a> 
                       <a href="/deletep/${pat.itrno}">Delete</a>                         
                       </td>                     
                       </tr>
                       </c:forEach>                     
                    </tbody>
                </table>
           </div>           
       </div>

    </div>

    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    
    <script>
        // MenuToggle
        let toggle = document.querySelector('.toggle');
        let navigation = document.querySelector('.navigation');
        let main = document.querySelector('.main');

        toggle.onclick = function()
        {
            navigation.classList.toggle('active');
            main.classList.toggle('active');
        }

        // add hovered class in selected list item
        let list = document.querySelectorAll('.navagation li');
        function activelink(){
            list.forEach((item) =>
            item.classlist.remove('hovered'));
            this.classlist.add('hovered')
        }
        list.forEach((item)=>
        item.addEventListener('mouseover',activelink));
    </script>
</body>
</html>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Patient Registeration Page</title>
    <link rel="stylesheet" href="/CSS/newpatient.css" type= "text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>

</head>
<body>
    <div class="container">
        <div class="regform">
            <div class="title"><span>${Title}</span></div>
                  <form action="/sucessfully-savep"  method="POST">
                    <div class="row">
                        <input type="number" id="itrno" name="itrno" placeholder="ITR NO" required>
                    </div>
                    <div class="row">
                        <input type="text" id="name" name="name" placeholder="Name" required>
                    </div>
                    <div class="row">
                        <input type="text" name="phone" id="phone" placeholder="Phone" required>
                    </div>
                    <div class="row">
                        <input type="text"  id="joindate" placeholder="Date " name="joindate" required>
                    </div>
                    <div class="row">
                        <input type="text"  id="address" placeholder="Address" name="address" required>    
                    </div>
                    <div class="row">
                        <input type="text" id="age" name ="age" placeholder="Age" required>
                    </div>
                    <div class="row">
                        <select class="option" id="gender" name="gender">
                            <option disabled="disabled" selected="selected">--Choose Gender--</option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>   
                    <div class="row">
                        <select class="option" id="status" name="status">
                            <option disabled="disabled" selected="selected">--Choose status--</option>
                            <option> Married</option>
                            <option> Un Married </option> 
                           </select>
                   </div>
                    <div class="row">
                        <input type="text" id="allergy" name ="allergy" placeholder="Allergy" required>
                    </div>   

                    <div class="row">
                        <input type="text" id="disease" name ="disease" placeholder="Disease" required>
                    </div>   
                    <div class="row">
                        <input type="text" id ="doctorname" name="doctorname" placeholder="Doctor Name" required>
                    </div>   
                    <div class="row">
                        <input type="text" id="weight" name ="weight" placeholder="Weight" required>
                    </div>   
                    <div class="row">
                        <input type="text" id="height" name="height" placeholder="Height" required>
                    </div>   
                    <div class="row">
                        <input type="text" id="bp" name="bp" placeholder="BP" required>
                    </div>
                    <div class="row button">
                        <input type="submit" value="Save">
                      </div>
                    
    
            </form>
        </div>
    
    </div>
</div>
  
</body>
</html>
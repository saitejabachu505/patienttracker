<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Doctor Registeration Page</title>
    <link rel="stylesheet" href="/CSS/docregistration.css" type= "text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>

</head>
<body>
    <div class="container">
        <div class="regform">
            <div class="title"><span>${Title}</span></div>
                  <form action="/sucessfully-save"  method="POST">
                  <div class="row">
                        <input type="number"  placeholder="Id" id="id" name="id" required>
                    </div>
                    <div class="row">
                        <input type="text"  placeholder="Name" id="name" name="name" required>
                    </div>
                    <div class="row">
                        <input type="text" name ="email"  id="email" placeholder="Email" required>
                    </div>
                    <div class="row">
                        <input type="text" name ="username" id="username" placeholder="username" required>
                    </div>
                    <div class="row">
                        <input type="text" name ="password" id="password" placeholder="password" required>
                    </div>
                    <div class="row">
                        <input type="text" name ="confirm_password" id="confirm_password" placeholder="Confirmed-password" required>
                    </div>
                    <div class="row">
                        <input type="text" name ="phone" id="phone" placeholder="Phone" required>
                    </div>   
                    <div class="row">
                        <select class="option" id="gender" name="gender">
                            <option disabled="disabled" selected="selected">--Choose Gender--</option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>   
                    <div class="row">
                        <select class="option" id="section" name="section">
                            <option disabled="disabled" selected="selected">--Choose section--</option>
                            <option>Dental</option>
                            <option>Fecalysis</option>
                            <option>Hemathology</option>
                            <option>Xray</option>
                            <option>Reahabilitation</option>
                            <option>Sputum</option>
                            <option>Urinalysis</option>
                            <option>Meternity</option>
                            <option>Other</option>
                        </select>
                    </div>
                    <div class="row">
                        <input type="number" id="adminid" name="adminid" placeholder="Admin id" required>
                    </div>
                    <div class="row button">
                        <input type="submit" value="Save">
                      </div>
            </form>
        </div>
    
    </div>
</div>
    
</body>
</html>
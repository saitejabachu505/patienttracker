package com.example.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.entity.Admin;


@Repository
public interface AdminRepository extends CrudRepository<Admin, Integer>
{
    public Admin save(Admin ad);

    public List<Admin> findAll();
    
    public Admin findById(int id);
    
    public Admin deleteById(int id);
    
    public boolean existsByEmail(@Param("email") String email);


}

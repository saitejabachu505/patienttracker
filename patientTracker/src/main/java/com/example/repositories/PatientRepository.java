package com.example.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.Patient;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Integer>
{
    public Patient save(Patient dr);

    public List<Patient> findAll();
    
    public Patient findById(int id);
    
    public Patient deleteById(int itrno);

}

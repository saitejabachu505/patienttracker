package com.example.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.entity.Doctor;

@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Integer>
{
    public Doctor save(Doctor dr);

    public List<Doctor> findAll();
    
    public Doctor findById(int id);
    
    public Doctor deleteById(int id);
    
    public boolean existsByEmail(@Param("email") String email);
    
}


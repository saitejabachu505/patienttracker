package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Patient
{
     @Id
     public int itrno;
     public String name;
     public String phone;
     public String joindate;
     public String address;
     public String age;
     public String gender;
     public String status;
     public String allergy;
     public String disease;
     public String doctorname;
     public String weight;
     public String height;
     public String bp;
     
     public  Patient()
     {
    	 
     }
     
     public  Patient(int itrno,String name,String phone,String joindate,String address,String age,String gender,String status,String allergy,String disease,String doctorname,String weight,String height,String bp)
     {
    	 this.itrno=itrno;
    	 this.name=name;
    	 this.phone=phone;
    	 this.joindate=joindate;
    	 this.address=address;
    	 this.age=age;
    	 this.gender=gender;
    	 this.status=status;
    	 this.allergy=allergy;
    	 this.disease=disease;
    	 this.doctorname=doctorname;
    	 this.weight=weight;
    	 this.height=height;
    	 this.bp=bp;

     }

     // getter setter
	public int getItrno() {
		return itrno;
	}
	public void setItrno(int itrno) {
		this.itrno = itrno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getJoindate() {
		return joindate;
	}
	public void setJoindate(String joindate) {
		this.joindate = joindate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAllergy() {
		return allergy;
	}
	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}
	public String getDisease() {
		return disease;
	}
	public void setDisease(String disease) {
		this.disease = disease;
	}
	public String getDoctorname() {
		return doctorname;
	}
	public void setDoctorname(String doctorname) {
		this.doctorname = doctorname;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}

	 public String toString()
	 {
		 return this.getItrno()+","+this.getName()+","+this.getDisease();
	 }

}

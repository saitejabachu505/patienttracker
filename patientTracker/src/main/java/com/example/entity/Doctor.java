package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Doctor 
{

 @Id
 private int id;
 private String name;
 private String email;
 private String username;
 private String password;
 private String confirm_password;
 private String phone;
 private String gender;
 private String section;
 private int adminid;
 
 public Doctor()
 {
	 
 }
 
 public Doctor(int id,String name,String email,String username,String password,String confirm_password,String phone,String gender,String section,int adminid)
 {
     super();
     this.id= id;
	 this.name = name;
	 this.email = email;
	 this.username = username;
	 this.password = password;
	 this.confirm_password = confirm_password;
	 this.phone = phone;
	 this.gender = gender;
	 this.section = section;
	 this.adminid = adminid;

 }

 //getter setter
 
public int getAdminid() {
	return adminid;
}
public void setAdminid(int adminid) {
	this.adminid = adminid;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getConfirm_password() {
	return confirm_password;
}
public void setConfirm_password(String confirm_password) {
	this.confirm_password = confirm_password;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getSection() {
	return section;
}
public void setSection(String section) {
	this.section = section;
}

public int getId() {
	return id;
}
 public String toString()
 {
	 return this.getId()+","+this.getName()+","+this.getEmail();
 }
}



package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.example.entity.Doctor;
import com.example.entity.Patient;
import com.example.repositories.PatientRepository;

@RestController
public class DoctorController 
{
	@Autowired
	PatientRepository patientRepository;

	
	@GetMapping(value="/doctor")
	public ModelAndView doctorpage()
	{
		List<Patient> listPatient = patientRepository.findAll();
        ModelAndView mv = new ModelAndView("doctorhome","ListPatient",listPatient);
		return mv;
	}
    
    @RequestMapping(value="/sucessfully-savep", method = RequestMethod.POST)
    public RedirectView handlePatient(@ModelAttribute Patient patient, HttpServletRequest request)
    {
    	System.out.println(patient);
    	patientRepository.save(patient);
    	RedirectView redirectView = new RedirectView();
    	redirectView.setUrl(request.getContextPath() + "/doctor");
    	return redirectView;
    }

	@GetMapping(value="/doctor/contact")
	public ModelAndView contactusview()
	{
        ModelAndView mv = new ModelAndView("contactus","Contact","Contact Us");
		return mv;
	}
	
	@GetMapping(value="/doctor/addpatient")
	public ModelAndView adddpatientview()
	{
        ModelAndView mv = new ModelAndView("newpatient","Title","Register Patient");
		return mv;
	}
	
	@GetMapping(value="/signout")
	public ModelAndView loginview()
	{
        ModelAndView mv = new ModelAndView("login","Title","Login Here");
		return mv;
	}

    @RequestMapping("/editp/{itrno}")
    public ModelAndView editDoctor(@PathVariable(name = "itrno") int itrno,Model model)
    {
    	Patient patient = patientRepository.findById(itrno);
    	System.out.println(patient);
    	model.addAttribute("patient",patient);
    	ModelAndView mv = new ModelAndView("updatePatient","Title","Update Patient");
    	return mv;
    }

    @RequestMapping("/deletep/{itrno}")
    public RedirectView deletePatient(@PathVariable(name = "itrno") int itrno, HttpServletRequest request)
    {
    	patientRepository.deleteById(itrno);
    	RedirectView redirectView = new RedirectView();
    	redirectView.setUrl(request.getContextPath() + "/doctor");
    	return redirectView;
    }


}

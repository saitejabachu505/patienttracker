package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.example.entity.Doctor;
import com.example.entity.Login;
import com.example.entity.Patient;
import com.example.repositories.AdminRepository;
import com.example.repositories.DoctorRepository;
import com.example.repositories.PatientRepository;

@RestController

public class LoginController 
{
	@Autowired
	DoctorRepository doctorRepository;
	@Autowired
	AdminRepository adminRepository;
	@Autowired
	PatientRepository patientRepository;

	
	@GetMapping(value="/login")
	public ModelAndView contactusview()
	{
        ModelAndView mv = new ModelAndView("login","Title","Login Here");
		return mv;
	}

    @RequestMapping(value="/sucessfully-login", method = RequestMethod.POST)
    public ModelAndView	loginhandller(@ModelAttribute Login login,Model model)
    {
    	 ModelAndView mv = null;
    	 String email = login.getEmail();
    	 String password = login.getPassword();
    	 
    	if(adminRepository.existsByEmail(email))
    	{
             List<Doctor> listDoctor =  doctorRepository.findAll();
    		 model.addAttribute("Admin",login);
    	     mv = new ModelAndView("admin","ListDoctor",listDoctor);
    	}
    	else if (doctorRepository.existsByEmail(email))
    	{
    		 List<Patient> listPatient =  patientRepository.findAll();
   		     model.addAttribute("Doctor",login);
   	         mv = new ModelAndView("doctorhome","ListPatient",listPatient);
    	}
    	else
    	{
        	 mv = new ModelAndView("login","Title","Login Here");
    	}
        return mv;
    }
    
}

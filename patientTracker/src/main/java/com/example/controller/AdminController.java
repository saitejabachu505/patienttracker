package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.example.entity.Admin;
import com.example.entity.Doctor;
import com.example.repositories.AdminRepository;
import com.example.repositories.DoctorRepository;

@RestController
public class AdminController 
{
	@Autowired
	DoctorRepository doctorRepository;
	@Autowired
	AdminRepository adminRepository;
	
	@GetMapping(value="/admin")
	public ModelAndView adminpage()
	{
        List<Doctor> listDoctor =  doctorRepository.findAll();
        for(Doctor doctor:listDoctor)
        {
        	System.out.println(doctor.getAdminid()+doctor.getName());
        }
        ModelAndView mv = new ModelAndView("admin","ListDoctor",listDoctor);
		return mv;
	}
	
	@GetMapping(value="/admin/getAdmin")
	public ModelAndView getalladmin()
	{
        List<Admin> listAdmin =  adminRepository.findAll();
        for(Admin admin:listAdmin)
        {
        	System.out.println(admin.getId()+admin.getName());
        }
        ModelAndView mv = new ModelAndView("getAdmins","ListAdmin",listAdmin);
		return mv;
	}

	@GetMapping(value="/admin/section")
	public ModelAndView sectionPage()
	{
        ModelAndView mv = new ModelAndView("section","D","Dental");
		return mv;
	}	
	@GetMapping(value="/admin/contactinfo")
	public ModelAndView contactusinfoview()
	{
        ModelAndView mv = new ModelAndView("contactusinfo","Title","Contact Us Info");
		return mv;
	}

	@GetMapping(value="/admin/contactinfo/contactreply")
	public ModelAndView contactusreplyview()
	{
        ModelAndView mv = new ModelAndView("contactusreply","Title","Reply to Request");
		return mv;
	}

	@GetMapping(value="/admin/addAdmin")
	public ModelAndView addadminPage()
	{
        ModelAndView mv = new ModelAndView("newadmin","Title","Register Here");
		return mv;
	}	

	@GetMapping(value="/admin/addDoctor")
	public ModelAndView adddoctorPage()
	{
        ModelAndView mv = new ModelAndView("doctorRegister","Title","Register Doctor");
		return mv;
	}	
	
    // admin-crud operations

    @RequestMapping(value="/sucessfully-savea", method = RequestMethod.POST)
    public RedirectView handleAdmin(@ModelAttribute Admin admin, HttpServletRequest request)
    {
    	System.out.println(admin);
    	adminRepository.save(admin);
    	RedirectView redirectView = new RedirectView();
    	redirectView.setUrl(request.getContextPath() + "/login");
    	return redirectView;
    }
    
    @RequestMapping(value="/sucessfully-updatea", method = RequestMethod.POST)
    public RedirectView handleUpdateAdmin(@ModelAttribute Admin admin, HttpServletRequest request)
    {
    	System.out.println(admin);
    	adminRepository.save(admin);
    	RedirectView redirectView = new RedirectView();
    	redirectView.setUrl(request.getContextPath() + "/admin/getAdmin");
    	return redirectView;
    }

    @RequestMapping("/edita/{id}")
    public ModelAndView editAdmin(@PathVariable(name = "id") int id,Model model)
    {
    	Admin admin = adminRepository.findById(id);
    	System.out.println(admin);
    	model.addAttribute("Admin",admin);
    	ModelAndView mv = new ModelAndView("updateAdmin","Title","Update Admin");
    	return mv;
    }
    
    @RequestMapping("/deletea/{id}")
    public RedirectView deleteAdmin(@PathVariable(name = "id") int id, HttpServletRequest request)
    {
    	adminRepository.deleteById(id);
    	RedirectView redirectView = new RedirectView();
    	redirectView.setUrl(request.getContextPath() + "/admin/getAdmin");
    	return redirectView;
    }
    


    // doctor-crud operations
    
    @RequestMapping(value="/sucessfully-save", method = RequestMethod.POST)
    public RedirectView handleDoctor(@ModelAttribute Doctor doctor, HttpServletRequest request)
    {
    	System.out.println(doctor);
    	doctorRepository.save(doctor);
    	RedirectView redirectView = new RedirectView();
    	redirectView.setUrl(request.getContextPath() + "/admin");
    	return redirectView;
    }
    
    @RequestMapping("/edit/{id}")
    public ModelAndView editDoctor(@PathVariable(name = "id") int id,Model model)
    {
    	Doctor doctor = doctorRepository.findById(id);
    	System.out.println(doctor);
    	model.addAttribute("doctor",doctor);
    	ModelAndView mv = new ModelAndView("updateDoctor","Title","Update Doctor");
    	return mv;
    }
    
    @RequestMapping("/delete/{id}")
    public RedirectView deleteDoctor(@PathVariable(name = "id") int id, HttpServletRequest request)
    {
    	doctorRepository.deleteById(id);
    	RedirectView redirectView = new RedirectView();
    	redirectView.setUrl(request.getContextPath() + "/admin");
    	return redirectView;
    }
    
}
